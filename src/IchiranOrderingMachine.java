import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IchiranOrderingMachine {
    private JPanel root;
    private JButton Ramenbutton;
    private JButton extraNoodlesButton;
    private JButton charSiuButton;
    private JButton kikurageButton;
    private JButton riceButton;
    private JButton boiledEggButton;
    private JTextArea ordereditems;
    private JRadioButton veryHardRadioButton;
    private JRadioButton normalRadioButton;
    private JRadioButton softRadioButton;
    private JRadioButton Normal2JradioButton;
    private JRadioButton strongJRadioButton;
    private JRadioButton mildRadioButton;
    private JButton checkOutButton;
    private JTextArea total;


    public static int totalPrice = 0; //this is a global variable for total price
    public static int flagForRamen = 0; //this is a global variable for flag when ordering ramen

    public static void main(String[] args) {
        JFrame frame = new JFrame("IchiranOrderingMachine");
        frame.setContentPane(new IchiranOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.getContentPane().setBackground(new Color(200,81,75));
    }

    public IchiranOrderingMachine() {

        ButtonGroup hardnessButtonGroup = new ButtonGroup();
        hardnessButtonGroup.add(veryHardRadioButton);
        hardnessButtonGroup.add(normalRadioButton);
        hardnessButtonGroup.add(softRadioButton);

        ButtonGroup tasteButtonGroup = new ButtonGroup();
        tasteButtonGroup.add(strongJRadioButton);
        tasteButtonGroup.add(Normal2JradioButton);
        tasteButtonGroup.add(mildRadioButton);

        //Button for extra noodle
        extraNoodlesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Extra Noodle",210);
            }
        });
        extraNoodlesButton.setIcon(new ImageIcon(
                this.getClass().getResource("kaedama.jpg")
        ));
        extraNoodlesButton.setVerticalTextPosition(SwingConstants.TOP);
        extraNoodlesButton.setHorizontalTextPosition(SwingConstants.CENTER);

        //Button for char-siu
        charSiuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Char-Siu",250);
            }
        });
        charSiuButton.setIcon(new ImageIcon(
                this.getClass().getResource("char-siu.jpg")
        ));
        charSiuButton.setVerticalTextPosition(SwingConstants.TOP);
        charSiuButton.setHorizontalTextPosition(SwingConstants.CENTER);


        //Button for kikurage
        kikurageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kikurage",120);
            }
        });
        kikurageButton.setIcon(new ImageIcon(
                this.getClass().getResource("kikurage.jpg")
        ));
        kikurageButton.setVerticalTextPosition(SwingConstants.TOP);
        kikurageButton.setHorizontalTextPosition(SwingConstants.CENTER);

        //Button for Rice
        riceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Rice",110);
            }
        });
        riceButton.setIcon(new ImageIcon(
                this.getClass().getResource("Rice.jpg")
        ));
        riceButton.setVerticalTextPosition(SwingConstants.TOP);
        riceButton.setHorizontalTextPosition(SwingConstants.CENTER);

        //Button for boiled egg
        boiledEggButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Boiled Eggs",130);
            }
        });
        boiledEggButton.setIcon(new ImageIcon(
                this.getClass().getResource("Boiledeggs.jpeg")
        ));
        boiledEggButton.setVerticalTextPosition(SwingConstants.TOP);
        boiledEggButton.setHorizontalTextPosition(SwingConstants.CENTER);

        //button for ramen
        Ramenbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",930);
            }
        });
        Ramenbutton.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.png")
        ));
        Ramenbutton.setVerticalTextPosition(SwingConstants.TOP);
        Ramenbutton.setHorizontalTextPosition(SwingConstants.CENTER);

        //Button for check out
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout(hardnessButtonGroup,tasteButtonGroup);
            }
        });


    }

    //method when ordering
    void order(String foodName,int price){
        String[] options = {"LARGE","MEDIUM","SMALL"};
        //choice for food's size
        int choice = JOptionPane.showOptionDialog(
                null,
                "Choose the Size",
                "Size",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                options,
                options[1]
        );
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order a " + options[choice] + " size of "+ foodName + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        //setting price when changing the size
        switch (choice){
            case 0:
                price+=50;
                break;
            case 2:
                price-=50;
                break;
        }
        //execute only if you press yes
        if(confirmation==0){
            JOptionPane.showMessageDialog(null, "Order for " + foodName + " received.");
            String currentText = ordereditems.getText();
            ordereditems.setText(currentText+foodName+ "(" + options[choice]+") " + price +" yen\n");
            totalPrice +=price;//update the total price
            total.setText(totalPrice +" yen");
            if(foodName.equals("Ramen")) flagForRamen=1;//update the flag(cuz ramen must be ordered)
        }
    }

    //method when checkout
    void checkout(ButtonGroup hardness,ButtonGroup taste) {
        ButtonModel hardRadio = hardness.getSelection();
        ButtonModel tasteRadio = taste.getSelection();
        //confirmation for ramen and options
        if((hardRadio==null) || (tasteRadio==null) || flagForRamen==0){
            JOptionPane.showMessageDialog(null, "please select [hardness] and [taste] and order Ramen");
        }
        else {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to checkout ?",
                    "checkout Confirmation",
                    JOptionPane.YES_NO_OPTION);
            //execute only if you press yes
            if (confirmation == 0) {
                //setting options for ramen
                String selectedHardness="";
                String selectedTaste="";
                if(hardRadio.equals(veryHardRadioButton.getModel())) selectedHardness="Very hard";
                else if(hardRadio.equals(normalRadioButton.getModel())) selectedHardness="Normal";
                else selectedHardness = "Soft";
                if(tasteRadio.equals(strongJRadioButton.getModel())) selectedTaste="Strong";
                else if(tasteRadio.equals(Normal2JradioButton.getModel())) selectedTaste="Normal";
                else selectedTaste="Mild";

                ImageIcon clerk = new ImageIcon(
                        "src/clerk.png");
                JOptionPane.showMessageDialog(null,
                        "Thank you!\nThe total price is \" " + totalPrice + "\" yen.\n" +
                        "The hardness of noodle is \""+ selectedHardness +"\". The taste of soup is \"" + selectedTaste + "\".",
                        "CheckOut",
                        JOptionPane.INFORMATION_MESSAGE,clerk);
                //erase the text
                ordereditems.setText("");
                //return to 0
                totalPrice = 0;
                flagForRamen = 0;
                //clear for RadioButton
                hardness.clearSelection();
                taste.clearSelection();
                total.setText("0 yen");
            }
        }
    }
}
